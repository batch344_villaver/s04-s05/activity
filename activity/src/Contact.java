import java.util.ArrayList;

public class Contact {

    // Properties
    private String name;
    private ArrayList<String> contactNumber;
    private ArrayList<String> address;


    // Constructors
    // default
    public Contact(){
        this.name = "Empty";
        this.contactNumber = new ArrayList<>();
        this.address = new ArrayList<>();
    };

    // parameterized
    public Contact(String name, ArrayList<String> contactNumber, ArrayList<String> address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }


    // Getters and Setters

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContactNumber() {
        return contactNumber;
    }

    public ArrayList<String> getAddress() {
        return address;
    }

}
