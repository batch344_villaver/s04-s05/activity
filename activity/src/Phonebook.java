import java.util.ArrayList;

public class Phonebook {

    // Instance variable
    private ArrayList<Contact> contacts;


    // Constructors
    // default
    public Phonebook() {
        this.contacts = new ArrayList<>();
    }

    // parameterized
    public void addContact(String name, ArrayList<String> contactNumber, ArrayList<String> address) {
        Contact newContact = new Contact(name, contactNumber, address);
        contacts.add(newContact);
    }


    // Getters and Setters
    // getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // setter
    public void addContact(Contact newContact) {
        contacts.add(newContact);
    }
}
