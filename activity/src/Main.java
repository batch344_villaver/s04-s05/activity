import java.util.ArrayList;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        ArrayList<String> johnContactNum = new ArrayList<>();
        johnContactNum.add("+639152468596");
        johnContactNum.add("+639228547963");

        ArrayList<String> johnAddress = new ArrayList<>();
        johnAddress.add("my home in Quezon City");
        johnAddress.add("my office in Makati City");

        ArrayList<String> janeContactNum = new ArrayList<>();
        janeContactNum.add("+639162148573");
        janeContactNum.add("+639173698541");

        ArrayList<String> janeAddress = new ArrayList<>();
        janeAddress.add("my home in Caloocan City");
        janeAddress.add("my office in Pasay City");

        Contact johnDoe = new Contact("John Doe", johnContactNum, johnAddress);
        Contact janeDoe = new Contact("Jane Doe", janeContactNum, janeAddress);

        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);


        ArrayList<Contact> contacts = phonebook.getContacts();

        if (contacts.isEmpty()) {
            System.out.println("The phonebook is empty");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getContactNumber()) {
                    System.out.println(number);
                }
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddress()) {
                    System.out.println(address);
                }
                System.out.println("====================");
            }
        }


    }
}